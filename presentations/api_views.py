from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Presentation

# from json import JSONEncoder


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        # "status",
        # "href",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


def api_list_presentations(request, conference_id):
    presentations = Presentation.objects.filter(conference=conference_id)
    return JsonResponse(
        {"presentations": presentations}, encoder=PresentationListEncoder
    )


class PresentationShowEncoder(
    ModelEncoder
):  # Same as PresentationDetailEncoder from the examples
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "synopsis",
        "created",
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}


def api_show_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    return JsonResponse(
        {"presentation": presentation}, encoder=PresentationShowEncoder
    )

    # return JsonResponse({
    #     "presenter_name": presentation.presenter_name,
    #     "company_name": presentation.company_name,
    #     "presenter_email": presentation.presenter_email,
    #     "title": presentation.title,
    #     "synopsis": presentation.synopsis,
    #     "created": presentation.created,
    #     "status": presentation.status.id,
    #     "conference": {
    #         "name": presentation.conference.name,
    #         "href": presentation.conference.get_api_url(),
    #     }
    # })


# from django.http import JsonResponse
# from common.json import ModelEncoder
# from .models import Presentation


# class PresentationListEncoder(ModelEncoder):
#     model = Presentation
#     properties = ["title"]

#     def get_extra_data(self, o):
#         return {"status": o.status.name}


# def api_list_presentations(request, conference_id):
#     presentations = Presentation.objects.filter(conference=conference_id)

#     return JsonResponse(
#         {"presentations": presentations}, encoder=PresentationListEncoder
#     )

#     """
#     Lists the presentation titles and the link to the
#     presentation for the specified conference id.

#     Returns a dictionary with a single key "presentations"
#     which is a list of presentation titles and URLS. Each
#     entry in the list is a dictionary that contains the
#     title of the presentation, the name of its status, and
#     the link to the presentation's information.

#     {
#         "presentations": [
#             {
#                 "title": presentation's title,
#                 "status": presentation's status name
#                 "href": URL to the presentation,
#             },
#             ...
#         ]
#     }
#     """
#     # presentations = [
#     #     {
#     #         "title": p.title,
#     #         "status": p.status.name,
#     #         "href": p.get_api_url(),
#     #     }
#     #     for p in Presentation.objects.filter(conference=conference_id)
#     # ]
#     # return JsonResponse({"presentations": presentations})


# def api_show_presentation(request, pk):
#     """
#     Returns the details for the Presentation model specified
#     by the pk parameter.

#     This should return a dictionary with the presenter's name,
#     their company name, the presenter's email, the title of
#     the presentation, the synopsis of the presentation, when
#     the presentation record was created, its status name, and
#     a dictionary that has the conference name and its URL

#     {
#         "presenter_name": the name of the presenter,
#         "company_name": the name of the presenter's company,
#         "presenter_email": the email address of the presenter,
#         "title": the title of the presentation,
#         "synopsis": the synopsis for the presentation,
#         "created": the date/time when the record was created,
#         "status": the name of the status for the presentation,
#         "conference": {
#             "name": the name of the conference,
#             "href": the URL to the conference,
#         }
#     }
#     """
#     pres_ = Presentation.objects.get(pk=pk)
#     return JsonResponse(
#         {
#             "presenter_name": pres_.presenter_name,
#             "company_name": pres_.company_name,
#             "title": pres_.title,
#             "synopsis": pres_.synopsis,
#             "created": pres_.created,
#             "status": pres_.status.name,
#             "conference": [
#                 {
#                     "name": pres_.conference.name,
#                     "href": pres_.get_api_url(),
#                 }
#             ],
#         }
#     )
